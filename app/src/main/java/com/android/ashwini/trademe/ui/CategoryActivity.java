package com.android.ashwini.trademe.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.fragments.CategoryFragment;
import com.android.ashwini.trademe.fragments.ListingFragment;
import com.android.ashwini.trademe.fragments.SubCategoryFragment;
import com.android.ashwini.trademe.listeners.OnCategorySelectedListener;
import com.android.ashwini.trademe.listeners.OnSubCategorySelectedListener;
import com.android.ashwini.trademe.presenter.AndroidCategoryPresenter;
import com.android.ashwini.trademe.presenter.CategoryPresenter;
import com.java.android.trademe.dto.GSONCategory;
import com.java.android.trademe.dto.GSONListing;

import java.util.ArrayList;

public class CategoryActivity extends BaseActivity implements OnCategorySelectedListener, OnSubCategorySelectedListener, VolleyResponseUI<ArrayList<GSONListing>> {

    private static final String TAG = CategoryActivity.class.getSimpleName();
    public static final String SUB_CATEGORIES = "sub_categories";
    public static final java.lang.String SUB_CATEGORIES_NAME = "subcategory_name";
    private CategoryPresenter presenter;

    @Override
    public void doOnCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_category);


        presenter = new AndroidCategoryPresenter(this, this);


        if (findViewById(R.id.fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            CategoryFragment categoryFragment = new CategoryFragment();

            categoryFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, categoryFragment).commit();
        }
    }

    public void onCategorySelected(GSONCategory selectedCategory) {


        SubCategoryFragment subCategoryFragment = (SubCategoryFragment)
                getSupportFragmentManager().findFragmentById(R.id.listing_fragment);
        Bundle bundle = new Bundle();
        bundle.putSerializable(SUB_CATEGORIES, selectedCategory.getSubcategories());
        bundle.putString(SUB_CATEGORIES_NAME, selectedCategory.getName());

        if (subCategoryFragment != null) {
            subCategoryFragment.setActivityTitle(selectedCategory.getName());
            subCategoryFragment.setArguments(bundle);
        } else {
            SubCategoryFragment newFragment = new SubCategoryFragment();
            newFragment.setArguments(bundle);
            newFragment.setActivityTitle(selectedCategory.getName());
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            transaction.commit();
        }

    }


    public void onResponse(ArrayList<GSONListing> list, String name) {

        Intent intent = new Intent(this, ListingActivity.class);
        intent.putExtra(ListingFragment.SELECTED_CATEGORY, list);
        intent.putExtra(ListingFragment.SELECTED_CATEGORY_NAME, name);

        startActivity(intent);

    }


    @Override
    public void onSubCategorySelected(GSONCategory selectedCategory) {
        presenter.onCategorySelected(selectedCategory, selectedCategory.getName());
    }
}
