package com.android.ashwini.trademe.ui;

import android.content.Intent;
import android.os.Bundle;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.presenter.AndroidSplashPresenter;
import com.android.ashwini.trademe.presenter.SplashPresenter;
import com.java.android.trademe.dto.GSONCategory;

import java.util.ArrayList;

public class SplashActivity extends BaseActivity implements VolleyResponseUI<ArrayList<GSONCategory>> {

    private static final String TAG = SplashActivity.class.getSimpleName();
    public static final String GENERAL_CATEGORIES = "general_categories";
    private SplashPresenter presenter;

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_splash);

        presenter = new AndroidSplashPresenter(this, this);

        presenter.onCreate();


    }

    @Override
    public void onResponse(ArrayList<GSONCategory> subcategories, String name) {
        Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
        intent.putExtra(GENERAL_CATEGORIES, subcategories);
        startActivity(intent);
        finish();
    }

}
