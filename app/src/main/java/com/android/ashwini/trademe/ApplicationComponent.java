package com.android.ashwini.trademe;

import com.android.ashwini.trademe.adapter.ListingsAdapter;
import com.android.ashwini.trademe.presenter.AbstractVolleyResponsePresenter;
import com.android.ashwini.trademe.ui.ListingDetailActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Dagger 2 Component
 */
@Singleton
@Component(modules = AppModule.class)
public interface ApplicationComponent {

    void injectFor(TradeMeApplication application);

    void injectFor(ListingsAdapter adapter);

    void injectFor(ListingDetailActivity listingDetailActivity);

    void injectFor(AbstractVolleyResponsePresenter presenter);
}
