package com.android.ashwini.trademe;

import android.app.Application;
import android.content.Context;

public class TradeMeApplication extends Application {

    private static final Object TAG = TradeMeApplication.class.getSimpleName();
    private ApplicationComponent appComponents;

    @Override
    public void onCreate() {
        super.onCreate();


        appComponents = createAppComponent(this);
        appComponents.injectFor(this);

    }

    protected AppComponent createAppComponent(TradeMeApplication application) {
        return DaggerAppComponent.Initializer.init(application);
    }

    public static ApplicationComponent getAppComponent(Context applicationContext) {
        return ((TradeMeApplication) applicationContext.getApplicationContext()).getAppComponent();
    }

    public ApplicationComponent getAppComponent() {
        return appComponents;
    }


}
