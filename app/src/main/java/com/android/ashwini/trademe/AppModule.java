package com.android.ashwini.trademe;


import com.android.ashwini.trademe.network.LruBitmapCache;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final TradeMeApplication application;

    public AppModule(TradeMeApplication application) {
        this.application = application;
    }

    @Singleton
    @Provides
    public RequestQueue getRequestQueue() {
        return Volley.newRequestQueue(application.getApplicationContext());
    }

    @Singleton
    @Provides
    public ImageLoader getImageLoader(RequestQueue requestQueue) {
        return new ImageLoader(requestQueue, new LruBitmapCache(application.getApplicationContext()));
    }
}
