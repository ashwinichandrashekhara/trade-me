package com.android.ashwini.trademe.ui;

import android.content.Intent;
import android.os.Bundle;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.fragments.ListingFragment;
import com.android.ashwini.trademe.listeners.OnListingSelectedListener;
import com.java.android.trademe.dto.GSONListing;

public class ListingActivity extends BaseActivity  implements OnListingSelectedListener {


    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_listing);

        ListingFragment listingFragment = new ListingFragment();

        listingFragment.setArguments(getIntent().getExtras());

        getSupportFragmentManager().beginTransaction()
                .add(R.id.listing_fragment_container, listingFragment).commit();
    }

      @Override
    public void onListingSelected(GSONListing listing) {

        Intent intent = new Intent(this, ListingDetailActivity.class);
        intent.putExtra(ListingDetailActivity.SELECTED_LISTING, listing);

        startActivity(intent);
    }

}
