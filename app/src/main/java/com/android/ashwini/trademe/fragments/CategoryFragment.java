package com.android.ashwini.trademe.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.adapter.CategoryAdapter;
import com.android.ashwini.trademe.listeners.OnCategorySelectedListener;
import com.android.ashwini.trademe.ui.SplashActivity;
import com.java.android.trademe.dto.GSONCategory;

import java.util.ArrayList;

public class CategoryFragment extends ListFragment {

    OnCategorySelectedListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<GSONCategory> subcategories = (ArrayList<GSONCategory>) getActivity().getIntent().getSerializableExtra(SplashActivity.GENERAL_CATEGORIES);
        setListAdapter(new CategoryAdapter(subcategories, getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();

        listener.setActionBarTitle(getString(R.string.categories));

        if (getFragmentManager().findFragmentById(R.id.listing_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (OnCategorySelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnCategorySelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onCategorySelected((GSONCategory) getListAdapter().getItem(position));

        getListView().setItemChecked(position, true);
    }

}
