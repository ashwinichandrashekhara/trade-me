package com.android.ashwini.trademe.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.TradeMeApplication;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.java.android.trademe.dto.GSONListing;

import java.util.ArrayList;

import javax.inject.Inject;

public class ListingsAdapter extends BaseAdapter {
    private final ArrayList<GSONListing> listings;
    private Context context;
    private final int layoutResourceId = R.layout.listing_item;
    @Inject
    ImageLoader imageLoader;

    public ListingsAdapter(ArrayList<GSONListing> listings, Context context) {
        this.listings = listings;
        this.context = context;
        TradeMeApplication.getAppComponent(context).injectFor(this);
    }

    @Override
    public int getCount() {
        return listings.size() > 20 ? 20 : listings.size();
    }

    @Override
    public GSONListing getItem(int i) {
        return listings.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(layoutResourceId, viewGroup, false);
        }

        TextView listing_name = (TextView) view.findViewById(R.id.listing_name);
        TextView pricing = (TextView) view.findViewById(R.id.pricing);
        TextView region_name = (TextView) view.findViewById(R.id.region_name);

        listing_name.setText(getItem(i).getTitle());
        pricing.setText(getItem(i).getPriceDisplay());
        region_name.setText(context.getString(R.string.region) + getItem(i).getRegion());

        NetworkImageView networkImage = (NetworkImageView) view.findViewById(R.id.network_image);
        networkImage.setDefaultImageResId(R.drawable.image_not_found);

        networkImage.setImageUrl(getItem(i).getPictureHref(), imageLoader);


        return view;
    }
}
