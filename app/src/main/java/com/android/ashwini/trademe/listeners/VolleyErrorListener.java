package com.android.ashwini.trademe.listeners;

import android.util.Log;

import com.android.ashwini.trademe.presenter.VolleyResponsePresenter;
import com.android.volley.Response;
import com.android.volley.VolleyError;

public class VolleyErrorListener implements Response.ErrorListener {

    private static final String TAG = VolleyErrorListener.class.getSimpleName();
    private final VolleyResponsePresenter presenter;

    public VolleyErrorListener(VolleyResponsePresenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "onErrorResponse " + error.getMessage());
        presenter.onErrorResponse(error);
    }
}
