package com.android.ashwini.trademe.presenter;

import com.java.android.trademe.dto.GSONCategory;

public interface CategoryPresenter extends VolleyResponsePresenter {

    void onCategorySelected(GSONCategory selectedCategory, String name);
}
