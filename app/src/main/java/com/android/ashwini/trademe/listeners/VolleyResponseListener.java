package com.android.ashwini.trademe.listeners;

import com.android.ashwini.trademe.presenter.VolleyResponsePresenter;
import com.android.volley.Response;

import org.json.JSONObject;

public class VolleyResponseListener implements Response.Listener<JSONObject> {
    private final VolleyResponsePresenter presenter;

    public VolleyResponseListener(VolleyResponsePresenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void onResponse(JSONObject response) {
        presenter.onResponse(response);
    }
}
