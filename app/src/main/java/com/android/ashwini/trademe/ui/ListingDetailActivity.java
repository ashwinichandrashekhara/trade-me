package com.android.ashwini.trademe.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.TradeMeApplication;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.java.android.trademe.dto.GSONListing;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ListingDetailActivity extends BaseActivity {


    public static final String SELECTED_LISTING = "selected_listing";
    GSONListing listing;
    @Inject
    ImageLoader imageLoader;

    @InjectView(R.id.listing_image)
    NetworkImageView networkImage;
    @InjectView(R.id.listing_title)
    TextView listingTitle;
    @InjectView(R.id.listing_id)
    TextView listingId;
    @InjectView(R.id.listing_end_date)
    TextView endDate;
    @InjectView(R.id.listing_start_date)
    TextView startDate;
    @InjectView(R.id.listing_start_price)
    TextView startPrice;
    @InjectView(R.id.listing_price_display)
    TextView priceDisplay;
    @InjectView(R.id.listing_region)
    TextView region;
    @InjectView(R.id.listing_suburb)
    TextView suburb;
    @InjectView(R.id.listing_buy_now)
    TextView buyNow;

    @Override
    protected void doOnCreate(Bundle savedInstanceState) {
        setContentView(R.layout.listing_detail_view);
        TradeMeApplication.getAppComponent(getApplicationContext()).injectFor(this);
        ButterKnife.inject(this);

        if (savedInstanceState == null) {
            listing = (GSONListing) getIntent().getSerializableExtra(SELECTED_LISTING);
        }
    }


    @Override
    public void doOnResume() {
        if (listing != null) {
            updateListingDetail(listing);
        }
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SELECTED_LISTING, listing);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        listing = (GSONListing) savedInstanceState.getSerializable(SELECTED_LISTING);
    }

    public void updateListingDetail(GSONListing selectedListing) {
        setActionBarTitle(selectedListing.getTitle());
        networkImage.setDefaultImageResId(R.drawable.image_not_found);

        networkImage.setImageUrl(selectedListing.getPictureHref(), imageLoader);


        this.listing = selectedListing;


        listingTitle.setText(selectedListing.getTitle());
        listingId.setText(getString(R.string.listing_id) + selectedListing.getListingId());
        endDate.setText(getString(R.string.end_date) + selectedListing.getEndDate());
        startDate.setText(getString(R.string.start_date) + selectedListing.getStartDate());
        startPrice.setText(getString(R.string.start_price) + selectedListing.getStartPrice());
        priceDisplay.setText(getString(R.string.price_display) + selectedListing.getPriceDisplay());
        region.setText(getString(R.string.region) + selectedListing.getRegion());
        suburb.setText(getString(R.string.suburb) + selectedListing.getSuburb());
        buyNow.setText(getString(R.string.buy_now) + selectedListing.getBuyNowPrice());
    }
}
