package com.android.ashwini.trademe.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.ashwini.trademe.TradeMeApplication;
import com.android.ashwini.trademe.listeners.VolleyErrorListener;
import com.android.ashwini.trademe.listeners.VolleyResponseListener;
import com.android.ashwini.trademe.ui.VolleyResponseUI;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import javax.inject.Inject;

public abstract class AbstractVolleyResponsePresenter implements VolleyResponsePresenter {

    private static final String TAG = AbstractVolleyResponsePresenter.class.getSimpleName();
    private final VolleyResponseUI ui;
    @Inject
    RequestQueue requestQueue;

    public AbstractVolleyResponsePresenter(VolleyResponseUI ui, Context context) {
        this.ui = ui;
        TradeMeApplication.getAppComponent(context).injectFor(this);
    }

    protected void sendCategoryRequest() {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, getUrl(), new VolleyResponseListener(this), new VolleyErrorListener(this));
        addToRequestQueue(jsObjRequest);
    }

    @NonNull
    protected abstract String getUrl();

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        requestQueue.add(req);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        ui.onErrorResponse(error);
    }
}
