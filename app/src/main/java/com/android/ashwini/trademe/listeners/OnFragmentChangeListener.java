package com.android.ashwini.trademe.listeners;

public interface OnFragmentChangeListener {

    void setActionBarTitle(String actionBarTitle);
}