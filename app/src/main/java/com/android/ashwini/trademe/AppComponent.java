package com.android.ashwini.trademe;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent extends ApplicationComponent {
    final class Initializer {
        public static AppComponent init(TradeMeApplication application) {
            return DaggerAppComponent.builder().appModule(new AppModule(application)).build();
        }
    }
}
