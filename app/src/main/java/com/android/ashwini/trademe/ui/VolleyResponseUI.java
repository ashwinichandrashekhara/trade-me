package com.android.ashwini.trademe.ui;

import com.android.volley.VolleyError;

public interface VolleyResponseUI<T> {

     void onResponse(T subcategories, String name);

     void onErrorResponse(VolleyError error);
}
