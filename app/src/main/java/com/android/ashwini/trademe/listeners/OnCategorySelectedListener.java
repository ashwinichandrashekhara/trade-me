package com.android.ashwini.trademe.listeners;

import com.java.android.trademe.dto.GSONCategory;

public interface OnCategorySelectedListener extends OnFragmentChangeListener{
    void onCategorySelected(GSONCategory selectedCategory);
}