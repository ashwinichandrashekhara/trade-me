package com.android.ashwini.trademe.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.ashwini.trademe.R;
import com.java.android.trademe.dto.GSONCategory;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter {
    private final ArrayList<GSONCategory> subcategories;
    private Context context;
    private final int layoutResourceId = R.layout.category_item;

    public CategoryAdapter(ArrayList<GSONCategory> subcategories, Context context) {
        this.subcategories = subcategories;
        this.context = context;
    }

    @Override
    public int getCount() {
        return subcategories.size();
    }

    @Override
    public GSONCategory getItem(int i) {
        return subcategories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(layoutResourceId, viewGroup, false);
        }

        TextView textView = (TextView) view.findViewById(R.id.category_name);

        textView.setText(getItem(i).getName());


        return view;
    }
}
