package com.android.ashwini.trademe.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.ashwini.trademe.network.Constants;
import com.android.ashwini.trademe.ui.VolleyResponseUI;
import com.java.android.trademe.dto.GSONCategory;
import com.java.android.trademe.dto.GSONListingRoot;
import com.java.android.trademe.parser.ListingParser;

import org.json.JSONObject;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

public class AndroidCategoryPresenter extends AbstractVolleyResponsePresenter implements CategoryPresenter {

    private static final String TAG = AndroidCategoryPresenter.class.getSimpleName();
    private final VolleyResponseUI ui;
    private String URL = "https://api.tmsandbox.co.nz/v1/Search/General.json?category=";
    private final OAuthConsumer consumer;
    private String signedUrl;
    private String name;

    public AndroidCategoryPresenter(VolleyResponseUI ui, Context context) {
        super(ui,context);
        this.ui = ui;
        consumer = getConsumer();
    }


    public void onCategorySelected(GSONCategory selectedCategory, String name) {

        this.name = name;
        sendCategoryRequest(selectedCategory);
    }

    private   OAuthConsumer getConsumer() {
        OAuthConsumer consumer = new CommonsHttpOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
        return consumer;
    }

    private void sendCategoryRequest(GSONCategory selectedCategory) {
        try {
            URL = URL + selectedCategory.getNumber();
            signedUrl = consumer.sign(URL);
            sendCategoryRequest();
        } catch (OAuthMessageSignerException e) {
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e) {
            e.printStackTrace();
        } catch (OAuthCommunicationException e) {
            e.printStackTrace();
        }

    }

    @NonNull
    @Override
    protected String getUrl() {
        return signedUrl;
    }

    public void onResponse(JSONObject response) {
        ListingParser parser = new ListingParser(response.toString());
        GSONListingRoot root = parser.parse();
        ui.onResponse(root.getList(),name);


    }
}
