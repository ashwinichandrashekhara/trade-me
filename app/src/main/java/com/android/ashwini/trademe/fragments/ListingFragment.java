/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.ashwini.trademe.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.adapter.ListingsAdapter;
import com.android.ashwini.trademe.listeners.OnListingSelectedListener;
import com.java.android.trademe.dto.GSONListing;

import java.util.ArrayList;

public class ListingFragment extends ListFragment {
    public static final String SELECTED_CATEGORY = "selected_category";
    public static final String SELECTED_CATEGORY_NAME = "selected_category_name";
    ArrayList<GSONListing> listings;
    private OnListingSelectedListener listener;
    private String categoryName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            listings = (ArrayList<GSONListing>) savedInstanceState.getSerializable(SELECTED_CATEGORY);
            categoryName = savedInstanceState.getString(SELECTED_CATEGORY_NAME);

            listener.setActionBarTitle(categoryName);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();
        if (args != null) {
            categoryName = args.getString(SELECTED_CATEGORY_NAME);
            updateListing((ArrayList<GSONListing>) args.getSerializable(SELECTED_CATEGORY));
        } else if (listings != null) {
            updateListing(listings);
            listener.setActionBarTitle(categoryName);

        } else {
            if (getView().getId() == R.id.listing_fragment) {
                setListShown(true);
                setEmptyText(getActivity().getString(R.string.select_category));
            } else if (getView().getId() == R.id.fragment_container) {
                setListShown(false);
            }
            listener.setActionBarTitle(categoryName);
        }
    }

    public void updateListing(ArrayList<GSONListing> listings) {
        listener.setActionBarTitle(categoryName);
        if (listings != null) {
            setListShown(true);
            this.listings = listings;

            if (listings.isEmpty()) {
                setEmptyText(getActivity().getString(R.string.no_listings));
            } else {
                setListAdapter(new ListingsAdapter(listings, getActivity()));
            }
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            updateListing((ArrayList<GSONListing>) savedInstanceState.getSerializable(SELECTED_CATEGORY));
            categoryName = savedInstanceState.getString(SELECTED_CATEGORY_NAME);
            listener.setActionBarTitle(categoryName);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (listings != null) {
            outState.putSerializable(SELECTED_CATEGORY, listings);
            outState.putString(SELECTED_CATEGORY_NAME, categoryName);
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnListingSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnListingSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onListingSelected((GSONListing) getListAdapter().getItem(position));

        getListView().setItemChecked(position, true);
    }

    public void setActivityTitle(String name) {

        this.categoryName = name;
    }
}