package com.android.ashwini.trademe.listeners;

import com.java.android.trademe.dto.GSONListing;

public interface OnListingSelectedListener extends OnFragmentChangeListener {
    void onListingSelected(GSONListing listing);

}