package com.android.ashwini.trademe.listeners;

import com.java.android.trademe.dto.GSONCategory;

public interface OnSubCategorySelectedListener extends OnFragmentChangeListener{
    void onSubCategorySelected(GSONCategory selectedCategory);
}