/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.ashwini.trademe.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.android.ashwini.trademe.R;
import com.android.ashwini.trademe.adapter.CategoryAdapter;
import com.android.ashwini.trademe.listeners.OnSubCategorySelectedListener;
import com.android.ashwini.trademe.ui.CategoryActivity;
import com.java.android.trademe.dto.GSONCategory;

import java.util.ArrayList;

public class SubCategoryFragment extends ListFragment {
    OnSubCategorySelectedListener listener;
    private String activityTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<GSONCategory> subcategories = (ArrayList<GSONCategory>)  getArguments().getSerializable(CategoryActivity.SUB_CATEGORIES);

        activityTitle = getArguments().getString(CategoryActivity.SUB_CATEGORIES_NAME);
        setListAdapter(new CategoryAdapter(subcategories, getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();

        listener.setActionBarTitle(getString(R.string.categories));

        if (getFragmentManager().findFragmentById(R.id.listing_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
        listener.setActionBarTitle(activityTitle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (OnSubCategorySelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnSubCategorySelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onSubCategorySelected((GSONCategory) getListAdapter().getItem(position));

        getListView().setItemChecked(position, true);
    }

    public void setActivityTitle(String activityTitle) {
       // this.activityTitle = activityTitle;
    }
}