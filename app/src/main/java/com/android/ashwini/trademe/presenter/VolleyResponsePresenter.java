package com.android.ashwini.trademe.presenter;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface VolleyResponsePresenter {

    void onResponse(JSONObject response);

    void onErrorResponse(VolleyError error);
}
