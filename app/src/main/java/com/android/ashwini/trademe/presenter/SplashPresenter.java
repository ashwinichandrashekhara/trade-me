package com.android.ashwini.trademe.presenter;

public interface SplashPresenter extends VolleyResponsePresenter {

    void onCreate();
}
