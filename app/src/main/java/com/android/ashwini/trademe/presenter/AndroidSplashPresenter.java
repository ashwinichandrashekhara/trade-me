package com.android.ashwini.trademe.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.ashwini.trademe.ui.VolleyResponseUI;
import com.java.android.trademe.dto.GSONCategoryRoot;
import com.java.android.trademe.parser.CategoryParser;

import org.json.JSONObject;

public class AndroidSplashPresenter extends AbstractVolleyResponsePresenter implements SplashPresenter {

    private static final String TAG = AndroidSplashPresenter.class.getSimpleName();
    private final String URL = "http://api.tmsandbox.co.nz/v1/Categories.json";
    private final VolleyResponseUI ui;

    public AndroidSplashPresenter(VolleyResponseUI ui, Context context) {
        super(ui, context);
        this.ui = ui;
    }

    @NonNull
    @Override
    protected String getUrl() {
        return URL;
    }

    public void onResponse(JSONObject response) {
        CategoryParser parser = new CategoryParser(response.toString());
        GSONCategoryRoot root = parser.parse();
        ui.onResponse(root.getSubcategories(), "");

    }

    @Override
    public void onCreate() {
        sendCategoryRequest();
    }
}
