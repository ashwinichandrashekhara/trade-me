package com.java.android.trademe.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class GSONListingRoot implements Serializable{

    private ArrayList<GSONListing> List;

    public ArrayList<GSONListing> getList() {
        return List;
    }
}
