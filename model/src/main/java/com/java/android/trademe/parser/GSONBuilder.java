package com.java.android.trademe.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

public class GSONBuilder {
    public Gson buildGeneralCategory() {

        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        String date = json.getAsJsonPrimitive().getAsString();
                        date = date.substring(6, date.length() - 2);
                        return new Date(Long.parseLong(date));
                    }
                }).create();
    }
}
