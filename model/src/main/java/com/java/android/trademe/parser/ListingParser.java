package com.java.android.trademe.parser;

import com.google.gson.Gson;
import com.java.android.trademe.dto.GSONListingRoot;

public class ListingParser {

    private final String json;

    public ListingParser(String json) {
        this.json = json;
    }

    public GSONListingRoot parse() {

        Gson gson = new GSONBuilder().buildGeneralCategory();
        GSONListingRoot listings = gson.fromJson(json, GSONListingRoot.class);
        return listings;
    }
}
