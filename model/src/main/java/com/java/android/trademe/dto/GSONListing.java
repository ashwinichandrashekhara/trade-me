package com.java.android.trademe.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GSONListing implements Serializable {

    private int ListingId;
    private String Title;

    private String PictureHref;

    private String Region;
    private String Suburb;

    private double StartPrice;
    private double BuyNowPrice;
    private String PriceDisplay;

    private Date StartDate;
    private Date EndDate;


    public int getListingId() {
        return ListingId;
    }

    public String getTitle() {
        return Title;
    }

    public String getPictureHref() {
        return PictureHref;
    }

    public String getRegion() {
        return Region;
    }

    public String getSuburb() {
        return Suburb;
    }

    public double getStartPrice() {
        return StartPrice;
    }

    public double getBuyNowPrice() {
        return BuyNowPrice;
    }

    public String getStartDate() {
        return getAsString(StartDate);
    }

    public String getEndDate() {
        return getAsString(EndDate);
    }

    private String getAsString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy", Locale.US);
        return sdf.format(date);
    }

    public String getPriceDisplay() {
        return PriceDisplay;
    }
}
