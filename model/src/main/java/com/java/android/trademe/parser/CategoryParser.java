package com.java.android.trademe.parser;

import com.google.gson.Gson;
import com.java.android.trademe.dto.GSONCategoryRoot;

public class CategoryParser {

    private final String json;

    public CategoryParser(String json) {
        this.json = json;
    }

    public GSONCategoryRoot parse() {

        Gson gson = new GSONBuilder().buildGeneralCategory();
        GSONCategoryRoot gsonCategoryRoot = gson.fromJson(json, GSONCategoryRoot.class);
        return gsonCategoryRoot;
    }
}
