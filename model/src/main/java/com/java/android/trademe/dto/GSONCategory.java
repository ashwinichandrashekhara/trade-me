package com.java.android.trademe.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class GSONCategory implements Serializable{

    private String Name;
    private String Number;
    private ArrayList<GSONCategory> Subcategories;

    public String getName() {
        return Name;
    }

    public String getNumber() {
        return Number;
    }

    public ArrayList<GSONCategory> getSubcategories() {
        return Subcategories;
    }
}
