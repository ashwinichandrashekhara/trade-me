package com.java.android.trademe.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class GSONCategoryRoot implements Serializable {

    private String Name;
    private String Number;
    private ArrayList<GSONCategory> Subcategories;

    public ArrayList<GSONCategory> getSubcategories() {
        return Subcategories;
    }

    public String getNumber() {
        return Number;
    }

    public String getName() {
        return Name;
    }
}
