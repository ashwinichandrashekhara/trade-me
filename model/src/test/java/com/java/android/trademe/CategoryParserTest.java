package com.java.android.trademe;

import com.google.common.io.CharStreams;
import com.java.android.trademe.dto.GSONCategoryRoot;
import com.java.android.trademe.parser.CategoryParser;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class CategoryParserTest {

    private String jsonFromWeb;
    CategoryParser parser;

    @Before
    public void before() throws IOException {

        jsonFromWeb = readFile("json_categories.json").trim();
        parser = new CategoryParser(jsonFromWeb);
    }

    @Test
    public void testDeserializeJsonString() throws Exception {

        GSONCategoryRoot root = parser.parse();

        assertEquals(root.getName(), "Root");
        assertEquals(root.getNumber(), "1");
        assertEquals(root.getSubcategories().size(), 4);

        assertEquals(root.getSubcategories().get(0).getName(), "Name1");
        assertEquals(root.getSubcategories().get(0).getNumber(), "Number1");
        assertEquals(root.getSubcategories().get(0).getSubcategories().size(), 2);

        assertEquals(root.getSubcategories().get(1).getName(), "Name2");
        assertEquals(root.getSubcategories().get(1).getNumber(), "Number2");
        assertEquals(root.getSubcategories().get(1).getSubcategories().size(), 3);

        assertEquals(root.getSubcategories().get(2).getName(), "Name3");
        assertEquals(root.getSubcategories().get(2).getNumber(), "Number3");
        assertEquals(root.getSubcategories().get(2).getSubcategories().size(), 4);

        assertEquals(root.getSubcategories().get(3).getName(), "Name4");
        assertEquals(root.getSubcategories().get(3).getNumber(), "Number4");
        assertEquals(root.getSubcategories().get(3).getSubcategories().size(), 2);


    }

    private String readFile(String name) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(name);
        return CharStreams.toString(new InputStreamReader(inputStream));
    }

}
