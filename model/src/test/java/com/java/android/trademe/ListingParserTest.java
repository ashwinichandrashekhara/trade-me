package com.java.android.trademe;

import com.google.common.io.CharStreams;
import com.java.android.trademe.dto.GSONListingRoot;
import com.java.android.trademe.parser.ListingParser;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class ListingParserTest {

    private String jsonFromWeb;
    ListingParser parser;

    @Before
    public void before() throws IOException {

        jsonFromWeb = readFile("json_listing.json").trim();
        parser = new ListingParser(jsonFromWeb);
    }

    @Test
    public void testDeserializeJsonString() throws Exception {

        GSONListingRoot root = parser.parse();

        assertEquals(root.getList().size(), 2);

        assertEquals(root.getList().get(0).getListingId(), 0);
        assertEquals(root.getList().get(0).getPictureHref(), "ABC");
        assertEquals(root.getList().get(0).getTitle(), "ABC");
        assertEquals(root.getList().get(0).getBuyNowPrice(), 0.0, 0.0);
        assertEquals(root.getList().get(0).getPriceDisplay(), "ABC");
        assertEquals(root.getList().get(0).getEndDate(), "01/Jan/2000");
        assertEquals(root.getList().get(0).getStartDate(), "01/Jan/2000");
        assertEquals(root.getList().get(0).getRegion(), "ABC");
        assertEquals(root.getList().get(0).getSuburb(), "ABC");
        assertEquals(root.getList().get(0).getStartPrice(), 0.0, 0.0);


        assertEquals(root.getList().get(1).getListingId(), 1);
        assertEquals(root.getList().get(1).getPictureHref(), "ABC1");
        assertEquals(root.getList().get(1).getTitle(), "ABC1");


    }

    private String readFile(String name) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(name);
        return CharStreams.toString(new InputStreamReader(inputStream));
    }

}
